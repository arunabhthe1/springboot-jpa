package com.springdbmysql.springmysqldemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
//@EnableJpaRepositories(basePackages = {"com.springdbmysql.mysqldemo.dao"})
//This @EnableJpaRepositories annotation is must when the dao/repository is not under the root class,
//with @SpringBootApplication which enables auto configuration
@SpringBootApplication
public class SpingMysqlDemoApplication {
	
	@Value("${app.test}")
	private static String applicationTest;
	
	public static void main(String[] args) {
		SpringApplication.run(SpingMysqlDemoApplication.class, args);
		System.out.println(applicationTest);
	}
	
	/* THis is the test commit.*/
	@Bean 
	public MessageSource messageSource() {
		
	    ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
	    messageSource.setBasenames("classpath:messages" , "classpath:error_messages");
	    messageSource.setCacheSeconds(10); //reload messages every 10 seconds
	    return messageSource;
	}

}

