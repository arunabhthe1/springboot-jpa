package com.springdbmysql.springmysqldemo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
@Entity
@Table(name = "UNDERLYING_INSTRUMENT")
public class UnderlyingInstrument {

	@Id
	@SequenceGenerator(name="und_inst_seq",sequenceName="underlying_instrument_sequence")        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="und_inst_seq") 	
	@Column(name="UND_INSTRUMENT_ID")
	private Long underlyingInstrumentId;
	@Column(name="UND_INSTRUMENT_NAME")
	private String underlyingInstrumentName;
	@ManyToOne
	@JoinColumn(name="INSTRUMENT_ID",referencedColumnName="INSTRUMENT_ID")
	private Instrument instrument;
	public Long getUnderlyingInstrumentId() {
		return underlyingInstrumentId;
	}
	public void setUnderlyingInstrumentId(Long underlyingInstrumentId) {
		this.underlyingInstrumentId = underlyingInstrumentId;
	}
	public String getUnderlyingInstrumentName() {
		return underlyingInstrumentName;
	}
	public void setUnderlyingInstrumentName(String underlyingInstrumentName) {
		this.underlyingInstrumentName = underlyingInstrumentName;
	}
	public Instrument getInstrument() {
		return instrument;
	}
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}
	
}
