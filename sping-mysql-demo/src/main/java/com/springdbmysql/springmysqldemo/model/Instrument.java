package com.springdbmysql.springmysqldemo.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "INSTRUMENT")
public class Instrument {
	@Id
	@SequenceGenerator(name="inst_seq",sequenceName="instrument_sequence")        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="inst_seq") 	
	@Column(name="INSTRUMENT_ID")
	private Long instrumentId;
	@Column(name="CUSIP")
	private String cusip;
	@Column(name="ISIN")
	private String isin;
	@Column(name="INSTRUMENT_NAME")
	private String instrumentName;
	//@OneToOne(cascade=CascadeType.PERSIST)
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="ISSUER_ID", referencedColumnName="ISSUER_ID")
	private Issuer issuer;
	
	@OneToMany(mappedBy="instrument",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private Set<UnderlyingInstrument> underlyingInstruments;
	
	public Long getInstrumentId() {
		return instrumentId;
	}
	public void setInstrumentId(Long instrumentId) {
		this.instrumentId = instrumentId;
	}
	public String getCusip() {
		return cusip;
	}
	public void setCusip(String cusip) {
		this.cusip = cusip;
	}
	public String getIsin() {
		return isin;
	}
	public void setIsin(String isin) {
		this.isin = isin;
	}
	public String getInstrumentName() {
		return instrumentName;
	}
	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}
	public Issuer getIssuer() {
		return issuer;
	}
	public void setIssuer(Issuer issuer) {
		this.issuer = issuer;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((instrumentId == null) ? 0 : instrumentId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Instrument other = (Instrument) obj;
		if (instrumentId == null) {
			if (other.instrumentId != null)
				return false;
		} else if (!instrumentId.equals(other.instrumentId))
			return false;
		return true;
	}
	
	
	

}
