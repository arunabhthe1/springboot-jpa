package com.springdbmysql.springmysqldemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "ISSUER")
public class Issuer {
	@Id
	@SequenceGenerator(name="iss_seq",sequenceName="issuer_sequence")        
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="iss_seq") 
	@Column(name="ISSUER_ID")
	private Long issuerId;
	@Column(name="ISSUER_NAME")
	private String issuerName;
	@Column(name="COUNTRY")
	private String country;
	public Long getIssuerId() {
		return issuerId;
	}
	public void setIssuerId(Long issuerId) {
		this.issuerId = issuerId;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((issuerId == null) ? 0 : issuerId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Issuer other = (Issuer) obj;
		if (issuerId == null) {
			if (other.issuerId != null)
				return false;
		} else if (!issuerId.equals(other.issuerId))
			return false;
		return true;
	}
	
	
}
