package com.springdbmysql.springmysqldemo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.springdbmysql.springmysqldemo.util.FakeDBDataSource;
import com.springdbmysql.springmysqldemo.util.FakeJMSDataSource;

@Configuration
@PropertySources(value = { @PropertySource("classpath:dbconfig.properties"), @PropertySource("classpath:jmsconfig.properties")})
public class PropertiesPlaceHolderConfig {
	@Value("${db.username}")
	private String dbusername;
	@Value("${db.password}")
	private String dbpassword;
	@Value("${db.url}")
	private String dburl;
	
	
	@Value("${jms.username}")
	private String jmsusername;
	@Value("${jms.username}")
	private String jmspasword;
	@Value("${jms.username}")
	private String jmsurl;
	
	
	@Bean
	public FakeDBDataSource fakeDBDataSource(){
		FakeDBDataSource dbsourse=new FakeDBDataSource();
		dbsourse.setUsername(dbusername);
		dbsourse.setPwd(dbpassword);
		dbsourse.setUrl(dburl);
		return dbsourse;
	}
	
	@Bean
	public FakeJMSDataSource fakeJMSDataSource(){
		FakeJMSDataSource jmssourse=new FakeJMSDataSource();
		jmssourse.setUsername(jmsusername);
		jmssourse.setPwd(jmspasword);
		jmssourse.setUrl(jmsurl);
		return jmssourse;
	}
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer properties(){
		return new PropertySourcesPlaceholderConfigurer();
	}
}
