package com.springdbmysql.springmysqldemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springdbmysql.springmysqldemo.dao.InstrumentRepository;
import com.springdbmysql.springmysqldemo.model.Instrument;

@RestController
@RequestMapping("/instrumentapp")

public class InstrumentController {
	@Autowired
	private InstrumentRepository instrumentRepository;
	@GetMapping(value="/instruments")
	public List<Instrument> getInstruments() {
		return instrumentRepository.findAll();
	}
	
	@PostMapping(value = "/instruments")
	public Instrument saveInstrument(@RequestBody Instrument instrument) {
		System.out.println(instrument);
		return instrumentRepository.save(instrument);
	}
	
	@PutMapping(value = "/instruments")
	public Instrument updateInstrument(@RequestBody Instrument instrument) {
		System.out.println(instrument);
		return instrumentRepository.save(instrument);
	}
}
