package com.springdbmysql.springmysqldemo.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.springdbmysql.springmysqldemo.dao.EmployeeRepository;
import com.springdbmysql.springmysqldemo.model.Employee;
import com.springdbmysql.springmysqldemo.util.FakeDBDataSource;
import com.springdbmysql.springmysqldemo.util.FakeJMSDataSource;

@RestController
@RequestMapping("/employeeapp")
public class EmployeeController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private FakeDBDataSource fakeDBDataSource;
	@Autowired
	private FakeJMSDataSource fakeJMSDataSource;

	public EmployeeController() {
		System.out.println("This is the EmployeeController");
	}

	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	// This is same as above
	// @GetMapping(value="/getAll")
	public List<Employee> getAllEmployees() {
		String message=messageSource.getMessage("app.test", new Object[0], new Locale("US"));
		System.out.println("message resource "+message);
		message=messageSource.getMessage("app.test", new Object[0], new Locale("EN"));//app.error.test
		System.out.println("message resource "+message);
		
		message=messageSource.getMessage("app.error.test", new Object[]{1,3}, new Locale("US"));
		System.out.println("message resource error message: "+message);
		
		System.out.println("fake datasourse name"+fakeDBDataSource.getUsername());
		System.out.println("fake datasourse url"+fakeDBDataSource.getUrl());
		System.out.println("fake jms datasourse name"+fakeJMSDataSource.getUsername());
		return employeeRepository.findAll();
	}

	// @RequestMapping(value="/saveEmp", method=RequestMethod.POST)
	// The above is same as @PostMapping
	@PostMapping(value = "/employees")
	public Employee saveEmployee(@RequestBody Employee emp) {
		System.out.println(emp);
		return employeeRepository.save(emp);
	}

	@GetMapping(value = "/employees/{empName}")
	public Employee getEmployeeByName(@PathVariable String empName) {
		return employeeRepository.findByEmpName(empName);
	}

	@PutMapping(value = "/employees")
	public Employee updateEmployee(@RequestBody Employee emp) {
		System.out.println(emp);
		return employeeRepository.save(emp);
	}

	@DeleteMapping(value = "/employees/{empid}")
	public void deleteEmployee(@PathVariable String empid) {

		employeeRepository.deleteById(Long.parseLong(empid));
	}
}
