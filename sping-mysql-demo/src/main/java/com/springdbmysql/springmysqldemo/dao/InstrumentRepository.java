package com.springdbmysql.springmysqldemo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springdbmysql.springmysqldemo.model.Instrument;

@Repository
public interface InstrumentRepository extends JpaRepository<Instrument,Long>{

}
